## Handover Bot
This 🤖 creates and assigns the triage issue to corresponding DRIs and pings the next week's DRIs as well, 
according to the Rotation Schedule. The issue is 
[scheduled](https://gitlab.com/gitlab-org/quality/pipeline-triage/-/pipeline_schedules) 
to open on Saturdays prior upcoming workweek.

It is also carrying over the issues from last week to contextualise the current DRI of past failures. 
The carryovers are issues that remained opened since reported on the previous week. 
These are automatically generated and included in the Pipeline Triage Issue.

## DRI Gem
Looking for some help triaging failures this week? Take a look at the 
[dri gem](https://gitlab.com/gitlab-org/quality/dri) to help reporting to this project your triaging progress.

## DRI weekly [rotation] schedule

|**Start Date**|**APAC [DRI]**|**EMEA [DRI]**|**AMER [DRI]**|
|:-|:-|:-|:-|
|2023-01-02|Jay McCure|Andrejs Cunskis|Richard Chong|
|2023-01-09|Mark Lapierre|Grant Young|Désirée Chevalier|
|2023-01-16|Harsha Muralidhar|Nick Westbury|Dan Davison|
|2023-01-23|Anastasia McDonald|Will Meek|Valerie Burton|
|2023-01-30|Carlo Catimbang|Alex Lyubenkov|Tiffany Rea|
|2023-02-06|Careem Ahamed|Nailia Iskhakova|Zeff Morgan|
|2023-02-13|Sanad Liaquat|John McDonnell|Sean Gregory|
|2023-02-20|Jay McCure|Sofia Vistas|Andy Hohenner|
|2023-02-27|Mark Lapierre|Edgars Brālītis|Richard Chong|
|2023-03-06|Harsha Muralidhar|Grant Young|Désirée Chevalier|
|2023-03-13|Anastasia McDonald|Nick Westbury|Dan Davison|
|2023-03-20|Sanad Liaquat|Will Meek|Tiffany Rea|
|2023-03-27|Mark Lapierre|Nailia Iskhakova|Valerie Burton|
|:point_right: **2023-04-03**|**Jay McCure**|**Andrejs Cunskis**|**Zeff Morgan**|
|2023-04-10|Harsha Muralidhar|John McDonnell|Sean Gregory|
|2023-04-17|Jay McCure|Edgars Brālītis|Andy Hohenner|
|2023-04-24|Sanad Liaquat|Sofia Vistas|Richard Chong|

[DRI]: https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/
[rotation]: https://about.gitlab.com/handbook/engineering/quality/quality-engineering/oncall-rotation/

_Any changes made to this `README.md` will get overwritten weekly by DRI highlighter task._

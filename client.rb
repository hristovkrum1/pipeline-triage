require 'gitlab'

class Client
  TRIAGE_PROJECT_ID = '15291320'
  API_URL = 'https://gitlab.com/api/v4'
  TOKEN = ENV['ACCESS_TOKEN']
  GITLAB_PROJECT_ID = '278964'

  def initialize
    message = "\e[31m[ERROR] ACCESS_TOKEN (your PAT) is missing. Please provide and retry.\e[0m"
    abort(message) if TOKEN.nil?
  end

  def gitlab
    Gitlab.client(
      endpoint: API_URL,
      private_token: TOKEN
    )
  end

  def get_user_id_by_handle(handle)
    gitlab.users(
      username: handle
    )
  end

  def get_latest_triage_issue
    issues = gitlab.issues(
      TRIAGE_PROJECT_ID,
      state: 'opened',
      order_by: 'created_at',
      scope: 'all'
    )
    issues.first
  end

  def get_notes_from_issue(iid)
    gitlab.issue_notes(
      TRIAGE_PROJECT_ID,
      iid
    )
  end

  def issue_opened?(iid)
    issue = gitlab.issues(
      GITLAB_PROJECT_ID,
      state: 'opened',
      scope: 'all',
      iids: iid
    )
    !issue.empty?
  end

  def open_triage_issue(title, template, assignees)
    gitlab.create_issue(
      TRIAGE_PROJECT_ID,
      title,
      description: template,
      assignee_ids: assignees
    )
  end

  def close_past_triage_report
    issues = gitlab.issues(
      TRIAGE_PROJECT_ID,
      state: 'opened',
      order_by: 'created_at',
      scope: 'all'
    )

    gitlab.close_issue(
      TRIAGE_PROJECT_ID,
      issues.last.to_h.fetch("iid")
    )
  end

  def create_branch(branch, ref)
    puts "Creating new branch - #{branch}..."

    gitlab.create_branch(TRIAGE_PROJECT_ID, branch, ref)
  end

  def create_commit(branch, message, actions)
    puts "Committing changes to branch #{branch}..."

    gitlab.create_commit(TRIAGE_PROJECT_ID, branch, message, actions)
  end

  def create_merge_request(mr_description, title, source_branch)
    puts "Creating merge request from branch - #{source_branch}..."

    options = {
      description: mr_description,
      source_branch: source_branch,
      target_branch: 'master',
      remove_source_branch: true,
      squash: true,
      labels: 'Quality'
    }

    mr = gitlab.create_merge_request(TRIAGE_PROJECT_ID, title, options)

    puts "\e[32mDONE!\e[0m"

    mr.to_hash
  end

  def accept_merge_request(mr_iid)
    puts "Merging MR with ID - #{mr_iid}..."
    gitlab.accept_merge_request(TRIAGE_PROJECT_ID, mr_iid)
  end
end

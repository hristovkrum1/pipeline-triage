# frozen_string_literal: true

require './client'
require 'markdown-tables'
require 'yaml'

# This script is to populate a new schedule for pipeline triage
#
# Example:
# |Start Date|APAC|EMEA|AMER|
# |:-|:-|:-|:-|
# |2022-08-22|Dan Davison|Sofia Vistas|Anastasia McDonald|
# |2022-08-29|Zeff Morgan|Nick Westbury|Careem Ahamed|
# |2022-09-05|Tiffany Rea|Andrejs Cunskis|Chloe Liu|
#
# The number of rows in table body is the number of Mondays between today and `no_of_days`.
# Default `no_of_days` is 90 days.
# Command:
# rake create_and_submit_schedule[<optional_no_of_days>]
# rake create_schedule_only[<optional_no_of_days>]

class Scheduling
  def initialize(no_of_days)
    @amer = []
    @emea = []
    @apac = []
    @data = []

    # Populate data as array of hashes
    # [{"name"=>"Sofia Vistas", "handle"=>"svistas", "region"=>"EMEA"}, {...}, ...]
    #
    @ic_list = YAML.load_file('SETs_name_list.yml')

    # Grab only the names from ic_list
    #
    @current_ic_names = @ic_list.each_with_object([]) do |hsh, arr|
      arr << hsh['name']
    end

    @no_of_days = no_of_days.to_i
  end

  def populate_schedule
    date_of_next_monday
    populate_date
    repopulate_ic_list
    sort_names_by_timezone

    { @amer => 3, @emea => 2, @apac => 1 }.each do |name_list, column|
      populate_name(name_list, column)
    end

    generate_table
  end

  # Create a merge request for the new changes
  # Required: Environment variable ACCESS_TOKEN=<your gitlab personal access token>
  #
  def submit_schedule
    @client = Client.new

    @client.create_branch(new_branch_name, 'master')
    @client.create_commit(new_branch_name, 'Update DRI schedule files', commit_actions)
    @client.create_merge_request(mr_description, mr_title, new_branch_name)
  end

  private

  # Grab the remain portion of the current schedule
  # Starting from the current week of the current schedule
  #
  def new_starting_point
    first_monday = @mondays.first
    current_schedule = YAML.load_file('new_dri_schedule.yml', permitted_classes: [Date])
    starting_point = current_schedule.select { |row| row[0] >= first_monday }

    # If there is no match, take the last 4 rows
    if starting_point.empty?
      starting_point = current_schedule.slice(current_schedule.length - 4 .. current_schedule.length - 1)
    end

    # removing the date, we only need the names
    starting_point.each do |row|
      row.delete(row[0])
    end.flatten!

    # remove names that are not in the current list
    starting_point.reject! { |name| !@current_ic_names.include?(name) }

    # reverse the order
    starting_point.reverse
  end

  # Move the ICs in new_starting_point to the top of @ic_list
  #
  def repopulate_ic_list
    new_starting_point.each do |name|
      ic_info = @ic_list.find { |ic| ic['name'] == name }
      @ic_list.unshift(@ic_list.delete(ic_info))
    end
  end

  # Pop the names into their own list by timezone
  #
  # @amer = ['Dan Davison', 'Zeff Morgan', ... ]
  #
  def sort_names_by_timezone
    @ic_list.each do |item|
      case item['region']
      when 'AMER'
        @amer << item['name']
      when 'EMEA'
        @emea << item['name']
      else
        @apac << item['name']
      end
    end
  end

  # Generate a list of dates for all up coming Mondays
  # within given date range - default is 90 days
  #
  # ['2022-08-08', '2022-08-15', '2022-08-22', ... ]
  #
  def date_of_next_monday
    # If today is between tuesday - friday (2 - 5)
    # Move back 1 week so that we can start with the Monday of the current week
    # If today is Saturday or Sunday take the Monday of the following week
    # If today is Monday, start at the same day
    today = Date.today
    start_date = today.wday.between?(2,5) ? today - 7 : today
    end_date = start_date + @no_of_days
    dates_by_weekday = (start_date..end_date).group_by(&:wday)
    @mondays = dates_by_weekday[1] # All Mondays within given date range
  end

  # Add the dates to the body of the table first
  #
  # [['2022-08-08'], [...], ... ]
  #
  def populate_date
    @mondays.each do |day|
      @data << [day]
    end
  end

  # Now add the names into each column accordingly
  #
  # The length of name list would not always be the same as the number of rows in the table
  # If we run out of names, start again from the beginning of the list
  #
  # [['2022-08-08', 'Andrejs Cunskis', 'Aleksandr Lyubenkov', 'Anastasia McDonald'], [...], ... ]
  #
  def populate_name(name_list, col)
    counter = 0
    use_counter = false
    @data.each_with_index do |row, i|
      last_index = name_list.length - 1

      # Start again at index 0 if last index was reached
      if i > (last_index)
        # Reset counter to 0 if last index was reached
        counter = counter > last_index ? 0 : counter
        index = counter
        use_counter = true
      else
        index = i
      end

      row[col] = name_list[index]
      counter += 1 if use_counter
    end
  end

  # Create the table in markdown format
  #
  # | Start date | APAC | EMEA | AMER |
  # | ---------- | ---- | ---- | ---- |
  # | date       | name | name | name |
  #
  def generate_table
    headers = ['**Start Date**', '**APAC [DRI]**', '**EMEA [DRI]**', '**AMER [DRI]**']

    # Align left
    @table = MarkdownTables.make_table(headers, @data, is_rows: true, align: %w[l l l l])
    puts "\nNew schedule from #{@data.first[0]} to #{@data.last[0]}\n\n#{@table}\n\n"
  end

  # Collect SETs gitlab handles
  #
  def qe_handles
    handles = []
    @ic_list.each { |item| handles << item['handle'] }

    handles
  end

  def commit_actions
    [
      {
        action: 'update',
        file_path: 'new_dri_schedule.yml',
        content: @data.to_yaml
      }
    ]
  end

  def new_branch_name
    "#{Date.today}-new-dri-schedule"
  end

  def mr_title
    "[#{Date.today}] Upcoming pipeline rotations schedule"
  end

  def mr_description
    handles = qe_handles.map { |x| "@#{x}"}.join(', ')

    <<~EOF
      This merge request is to update the current pipeline rotations schedule with new schedule 
      for the upcoming months. This request is generated quarterly by `handover bot` in a [scheduled pipeline].

      ### [Tentative] New Schedule from **#{@data.first[0]}** to **#{@data.last[0]}**

      #{@table}

      _Any changes made to `new_dri_schedule.yml` in this MR is not reflected in this description automatically._

      ### To-do
      - SETs please make any necessary changes accordingly to your availability. It is **important** that we have the 
      correct schedule in [new_dri_schedule.yml] since [handover], [highlighter] and [chatops dri command] all read 
      from this file. It is optional to also update the table above, but it can be helpful for other reviewers.
      - SETs please let your QEM know when you have finished reviewing.
      - QEMs please double check for any scheduling errors; e.g. when your SETs are OOO and could not review/make
      changes in time, etc... . Once reviewing is complete, it is QEMs' responsibility to merge this MR.

      ### Note
      - The new [pipeline rotations schedule] might not be reflected right away after this MR is merged. Don't be 
      alarmed. [highlighter] will make the update by the end of the week (counting from the day of merging).
      - Please don't make any changes to [README.md] in or outside of this MR. [highlighter] will read from 
      [new_dri_schedule.yml] weekly and update/highlight the schedule accordingly. Any changes made to the file
      will be overwritten by [highlighter].
      - [new_dri_schedule.yml] is overwritten quarterly by [scheduler] so swapping/changing schedule in this
      file is also temporary. If there is need for permanent changes, please make the change in 
      [SETs_name_list.yml]. This change will be reflected in the next [scheduler] run or you can run 
      `rake create_schedule_only[120]` locally and commit the changes here.
      - The new schedule is created on the first day of the quarter in a calendar year (Jan, Apr, Jul, Oct). If you
      will be OOO during this time, it's good to let your QEMs know to do the necessary swapping for you.

      Happy scheduling :information_desk_person:

      cc #{handles}, @gl-quality/qe-managers

      [pipeline rotations schedule]: https://gitlab.com/gitlab-org/quality/pipeline-triage#dri-weekly-rotation-schedule
      [README.md]: README.md
      [new_dri_schedule.yml]: new_dri_schedule.yml
      [SETs_name_list.yml]: SETs_name_list.yml
      [handover]: handover.rb
      [scheduler]: scheduling.rb
      [highlighter]: dri_highlighter.rb
      [chatops dri command]: https://gitlab.com/gitlab-com/chatops/-/blob/master/lib/chatops/commands/quality.rb
      [scheduled pipeline]: https://gitlab.com/gitlab-org/quality/pipeline-triage/-/pipeline_schedules
    EOF
  end
end

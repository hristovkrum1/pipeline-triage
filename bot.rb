#!/usr/bin/env ruby

require './handover'

handover = Handover.new

current_dris = handover.current_dris
next_dris = handover.next_dris
assignees = handover.assignee_ids
timezone_dris = handover.timezone_dris_table
carryovers = handover.carryovers

puts "🤖: This week's expected DRIs are: #{current_dris.map { |k, v| v[:handle] }}"
puts "🤖: Next week's expected DRIs are: #{next_dris.map { |k, v| v[:handle] }}"
puts "🤖: There are no carryovers from last week..." if carryovers.empty?
puts "🤖: There are #{carryovers.size} carryovers from last week"

template = <<-EOF
# DRI

Please review the [responsiblities](https://about.gitlab.com/handbook/engineering/quality/quality-engineering/oncall-rotation/#responsibility) and [guidelines](https://about.gitlab.com/handbook/engineering/quality/quality-engineering/debugging-qa-test-failures/) if you have not done so recently.

Above all else, please remember that the aim of pipeline triage is to identify problems and try to get them **resolved** (ideally) _before_ they impact users.

You can view the [Testcases project list of issues for non-quarantined test results](https://gitlab.com/gitlab-org/quality/testcases/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=status%3A%3Aautomated&not[label_name][]=quarantine) to help identify failures and see if any were missed when reviewing pipelines.

Feel free to use the [dri gem](https://gitlab.com/gitlab-org/quality/dri) to help report and triage issues.

#{timezone_dris}

# Carryovers

#{carryovers.join("\n")}

# [Current known issues for CustomersDot project](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Quality%3Ae2e-test&label_name[]=staging%3A%3Afailed)

/label ~Quality

/assign
EOF

handover.new_triage_report(template, assignees)
puts "🤖: Hooray, new triage report open at https://gitlab.com/gitlab-org/quality/pipeline-triage/-/issues 🎉"

handover.close_past_triage_report
puts "🤖: Closed last week triage issue. Bye 👋"
